﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using SimpleHttp;

namespace QueueService
{
    public partial class serviceFrm : Form
    {
        public string KIOSK_CODE  = System.Configuration.ConfigurationManager.AppSettings["KIOSK_CODE"];
        public string AGENCY_CODE = System.Configuration.ConfigurationManager.AppSettings["AGENCY_CODE"];
        public string URL         = System.Configuration.ConfigurationManager.AppSettings["URL"];
        public string topMost     = System.Configuration.ConfigurationManager.AppSettings["TopMost"];
        public string showCursor  = System.Configuration.ConfigurationManager.AppSettings["ShowCursor"];
        public string fullScreen  = System.Configuration.ConfigurationManager.AppSettings["FullScreen"];

        public string IP_SERVER = System.Configuration.ConfigurationManager.AppSettings["IP_SERVER"];
        public string PORT      = System.Configuration.ConfigurationManager.AppSettings["PORT"];

        public ChromiumWebBrowser chromBrowser;

        public serviceFrm()
        {
            if (showCursor != null && showCursor.Equals("N"))
            {
                Cursor.Hide();
            }

            if (topMost != null && topMost.Equals("Y"))
            {
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }

            InitializeComponent();
            InitialzeChormium();      
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();

            if (fullScreen.Equals("Y"))
            {
                try
                {
                    GC.Collect();
                    Screen[] screens = Screen.AllScreens;

                    int screensNum = 0;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.StartPosition = FormStartPosition.Manual;
                                this.Size = new Size(bounds.Width, bounds.Height);
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.StartPosition = FormStartPosition.Manual;
                            this.Size = new Size(bounds.Width, bounds.Height);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "IQServiceFormH");
                }

            }
        }

        private void InitialzeChormium()
        {
            CefSettings settings = new CefSettings();

            settings.CefCommandLineArgs.Add("enable-media-stream", "1");

            Cef.Initialize(settings);

            URL = URL + "?code=" + KIOSK_CODE + "&bcode=" + AGENCY_CODE;

            Console.WriteLine(URL);


            

            chromBrowser = new ChromiumWebBrowser(URL);
            chromBrowser.Dock = DockStyle.Fill;

            chromBrowser.JavascriptObjectRepository.Settings.LegacyBindingEnabled = true;
            chromBrowser.JavascriptObjectRepository.Register("boundAsync", new AsyncBoundObject(), isAsync: true, options: BindingOptions.DefaultBinder);
            
            this.Controls.Add(chromBrowser);

            //TEST();
        }

        public class AsyncBoundObject
        {
            public void printQueue(String path)
            {
                //MessageBox.Show(path, "Test");
                Process p = new Process();
                p.StartInfo = new ProcessStartInfo()
                {
                    CreateNoWindow = true,
                    Verb = "print",
                    FileName = path //put the correct path here
                };
                p.Start();
            }

            public void closeApp()
            {
                string processName = "QueueService";

                foreach (Process process in Process.GetProcessesByName(processName))
                {
                    process.Kill();
                }
            }
        }




        private void startService()
        {
            //Console.WriteLine("service Start");
            try
            {
                Route.Add("/", (req, res, props) =>
                {
                    res.AsText("Welcome to the Queue Http Server");
                });

                Route.Add("/ws/", (req, res, props) =>
                {
                    res.AsText("Welcome to the Queue Http Server");
                });

                Route.Add("/ws/shutDown", (req, res, props) =>
                {
                    Utils.Shutdown();
                });

                Route.Add("/ws/printQueue/{path}", (req, res, props) =>
                {
                    string path = props["path"];
                    //MessageBox.Show(path);

                    path = System.Uri.UnescapeDataString(path);

                    Process p = new Process();
                    p.StartInfo = new ProcessStartInfo()
                    {
                        CreateNoWindow = true,
                        Verb = "print",
                        FileName = path //put the correct path here
                    };
                    p.Start();

                    res.AsText("success");
                });

                Route.Add("/ws/GetPerformanceInfo/{kioskCode}", (req, res, props) =>
                {
                    string kioskCode = props["kioskCode"];
                    //var data = new StringContent(PerformanceInfo.GetPerformanceInfo(), Encoding.UTF8, "application/json"); 
                    //res.AsText(PerformanceInfo.GetPerformanceInfo(), "application/json");

                    byte[] data = Encoding.UTF8.GetBytes(PerformanceInfo.GetPerformanceInfo());

                    res.ContentType = "application/json";
                    res.ContentEncoding = Encoding.UTF8;
                    res.ContentLength64 = data.LongLength;

                    // Write out to the response stream (asynchronously), then close it
                    res.OutputStream.WriteAsync(data, 0, data.Length);
                });

                HttpServer.ListenAsync(
                int.Parse(PORT),
                CancellationToken.None,
                Route.OnHttpRequestAsync

                ).Wait();
            }
            catch (Exception ex)
            {
                setNetSH(PORT);
                Thread.Sleep(2000);
                startService();
                Console.WriteLine(ex.ToString());
            }
        }


        private void setNetSH(string port)
        {
            string str = "netsh http add urlacl url=http://+:" + PORT + "/ user=\"Everyone\"";

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + str;
            startInfo.Verb = "runas";
            process.StartInfo = startInfo;
            process.Start();
        }

        private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        {
            Console.WriteLine("backgroundWorker1");
            CheckForIllegalCrossThreadCalls = false;
            startService();
        }

        private void panel1_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

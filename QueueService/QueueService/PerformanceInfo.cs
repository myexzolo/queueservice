﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Management;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace QueueService
{
    class PerformanceInfo
    {
        [DllImport("psapi.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetPerformanceInfo([Out] out PerformanceInformation PerformanceInformation, [In] int Size);

        [StructLayout(LayoutKind.Sequential)]
        public struct PerformanceInformation
        {
            public int Size;
            public IntPtr CommitTotal;
            public IntPtr CommitLimit;
            public IntPtr CommitPeak;
            public IntPtr PhysicalTotal;
            public IntPtr PhysicalAvailable;
            public IntPtr SystemCache;
            public IntPtr KernelTotal;
            public IntPtr KernelPaged;
            public IntPtr KernelNonPaged;
            public IntPtr PageSize;
            public int HandlesCount;
            public int ProcessCount;
            public int ThreadCount;
        }

        public static Int64 GetPhysicalAvailableMemoryInMiB()
        {
            PerformanceInformation pi = new PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
            {
                return Convert.ToInt64((pi.PhysicalAvailable.ToInt64() * pi.PageSize.ToInt64() / 1048576));
            }
            else
            {
                return -1;
            }

        }

        public static Int64 GetTotalMemoryInMiB()
        {
            PerformanceInformation pi = new PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
            {
                return Convert.ToInt64((pi.PhysicalTotal.ToInt64() * pi.PageSize.ToInt64() / 1048576));
            }
            else
            {
                return -1;
            }

        }

        public static string getCurrentCpuUsage()
        {
            PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            return cpuCounter.NextValue().ToString();
        }


        public static string getAvailableRAM()
        {
            PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            return ramCounter.NextValue().ToString();
        }
        public static string getUnitRAM()
        {
            return "MB";
        }

        public static List<string> GetDrivesInfo()
        {
            var drives = DriveInfo.GetDrives();
            List<string> str = new List<string>();

            foreach (DriveInfo info in drives)
            {

                //if (textBox1.InvokeRequired)
                //{
                //    textBox1.Invoke((MethodInvoker)delegate {
                //        textBox1.AppendText("==============================");
                //        textBox1.AppendText(Environment.NewLine);
                //        textBox1.AppendText("Name: " + info.Name);
                //        textBox1.AppendText(Environment.NewLine);
                //        textBox1.AppendText("Size: " + info.TotalSize);
                //        textBox1.AppendText(Environment.NewLine);
                //        textBox1.AppendText("TotalFreeSpace: " + info.TotalFreeSpace);
                //        textBox1.AppendText(Environment.NewLine);
                //        textBox1.AppendText("Drive Format: " + info.DriveFormat);
                //        textBox1.AppendText(Environment.NewLine);
                //        textBox1.AppendText("Drive Format: " + info.DriveFormat);
                //        textBox1.AppendText(Environment.NewLine);
                //    });
                //}
                try
                {
                    var infoDriver = new
                    {
                        Name = info.Name.ToString(),
                        Size = info.TotalSize.ToString(),
                        TotalFreeSpace = info.TotalFreeSpace.ToString(),
                        DriveFormat = info.DriveFormat.ToString()
                    };

                    str.Add(infoDriver.ToString());

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return str;
        }

        public static List<string> checkPrinterStatus()
        {
            List<string> str = new List<string>();

            string query = string.Format("SELECT * from Win32_Printer");
            System.Management.ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);

            foreach (ManagementObject service in searcher.Get())
            {
                string[] pStatus = { "", "Other", "Unknown", "Idle", "Printing", "WarmUp", "Stopped Printing", "Offline" };

                string[] pState = {"Idle", "Paused", "Error", "Pending Deletion", "Paper Jam", "Paper Out", "Manual Feed", "Paper Problem",
                                    "Offline", "IO Active", "Busy", "Printing", "Output Bin Full", "Not Available", "Waiting",
                                    "Processing", "Initialization", "Warming Up", "Toner Low", "No Toner", "Page Punt",
                                    "User Intervention Required", "Out of Memory", "Door Open", "Server_Unknown", "Power Save"};

                string result = "";
                foreach (System.Management.PropertyData pData in service.Properties)
                {
                    if (pData.Name == "Caption" || pData.Name == "Default" || pData.Name == "ServerName")
                    {
                        result += pData.Name + " = " + pData.Value + " ; ";
                    }
                    else if (pData.Name == "PrinterState")
                    {
                        if (Convert.ToInt32(pData.Value) < 26)
                        {
                            result += pData.Name + " = " + pState[Convert.ToInt32(pData.Value)] + " ; ";
                        }
                        else
                        {
                            result += pData.Name + " = ; ";
                        }

                    }
                    else if (pData.Name == "PrinterStatus")
                    {
                        result += pData.Name + " = " + pStatus[Convert.ToInt32(pData.Value)] + " ; ";
                    }
                }
                if (result != "")
                {
                    str.Add(result);
                }
            }

            return str;   // the printer status shows "Idle", printer itself "Out of Paper"
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public static string GetPerformanceInfo()
        {
            string json = "";

            Int64 phav = PerformanceInfo.GetPhysicalAvailableMemoryInMiB();
            Int64 tot = PerformanceInfo.GetTotalMemoryInMiB();
            int percentFree = (int)Math.Ceiling(((Double)phav / (Double)tot) * 100);
            int percentOccupied = 100 - percentFree;

            var cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            cpuCounter.NextValue();
            Thread.Sleep(1000);


            var Mem = new
            {
                PhysicalAvailableMemoryInMiB = phav,
                TotalMemoryInMiB = tot,
                percentFree = percentFree,
                percentOccupied = percentOccupied
            };

            var Cpu = new
            {
                Total = cpuCounter.NextValue(),
            };



            var IpA = new
            {
                IP_Address = GetLocalIPAddress()
            };


            var performanceInfo = new
            {
                Memory = Mem,
                Cpu = Cpu,
                IP_Address = IpA,
                DrivesInfo = GetDrivesInfo(),
                PrinterStatus = checkPrinterStatus()

            };

            json = JsonConvert.SerializeObject(performanceInfo);
            return json;
        }

    }
}

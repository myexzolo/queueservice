﻿using SimpleHttp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QueueAgent
{
    public partial class frm : Form
    {
        public string KIOSK_CODE = System.Configuration.ConfigurationManager.AppSettings["KIOSK_CODE"];
        public string IP_SERVER = System.Configuration.ConfigurationManager.AppSettings["IP_SERVER"];
        public string PORT = System.Configuration.ConfigurationManager.AppSettings["PORT"];
        public frm()
        {
            InitializeComponent();

            
        }

        private void frm_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            startService();
        }

        private void setNetSH(string port)
        {
            string str = "netsh http add urlacl url=http://+:" + PORT + "/ user=\"Everyone\"";

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + str;
            startInfo.Verb = "runas";
            process.StartInfo = startInfo;
            process.Start();
        }

        private void startService()
        {
            try
            {
                Route.Add("/", (req, res, props) =>
                {
                    res.AsText("Welcome to the Queue Http Server");
                });

                Route.Add("/ws/", (req, res, props) =>
                {
                    res.AsText("Welcome to the Queue Http Server");
                });

                Route.Add("/ws/printQueue/{path}", (req, res, props) =>
                {
                    string path = props["path"];
                    //MessageBox.Show(path);
                    Task.Run(() =>
                    {
                        Process p = new Process();
                        p.StartInfo = new ProcessStartInfo()
                        {
                            CreateNoWindow = true,
                            Verb = "print",
                            FileName = path //put the correct path here
                        };
                        p.Start();

                        res.AsText("success");
                    });
                });

                Route.Add("/ws/GetPerformanceInfo/{kioskCode}", (req, res, props) =>
                {
                    Task.Run(() =>
                    {
                        string kioskCode = props["kioskCode"];
                        //var data = new StringContent(PerformanceInfo.GetPerformanceInfo(), Encoding.UTF8, "application/json"); 
                        //res.AsText(PerformanceInfo.GetPerformanceInfo(), "application/json");

                        byte[] data = Encoding.UTF8.GetBytes(PerformanceInfo.GetPerformanceInfo());

                        res.ContentType = "application/json";
                        res.ContentEncoding = Encoding.UTF8;
                        res.ContentLength64 = data.LongLength;

                        // Write out to the response stream (asynchronously), then close it
                        res.OutputStream.WriteAsync(data, 0, data.Length);
                    });
                });



                HttpServer.ListenAsync(
                int.Parse(PORT),
                CancellationToken.None,
                Route.OnHttpRequestAsync

                ).Wait();
            }
            catch (Exception ex)
            {
                setNetSH(PORT);
                Thread.Sleep(2000);
                startService();
                Console.WriteLine(ex.ToString());
            }
        }

       
    }
}

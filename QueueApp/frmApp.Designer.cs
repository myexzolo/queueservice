﻿
namespace QueueApp
{
    partial class frmApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmApp));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTransfer = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.labKeep = new System.Windows.Forms.Label();
            this.btnKeep = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnEnd = new System.Windows.Forms.Button();
            this.btnCall = new System.Windows.Forms.Button();
            this.btnRecursive = new System.Windows.Forms.Button();
            this.labQueueCode = new System.Windows.Forms.Label();
            this.labService = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labKpi = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labWait = new System.Windows.Forms.Label();
            this.labCounter = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labTimeService = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.nextBtn = new System.Windows.Forms.Button();
            this.prevBtn = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.HotKeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.callToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RecursiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EndToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.KeepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ปดระบบToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.เกบควToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ตงคาToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tooTopMostlStripComboBox1 = new System.Windows.Forms.ToolStripMenuItem();
            this.autoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.แจงเตอนToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.callToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.EndToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.CancelToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.KeepToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.รายการเกบควToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.แจงเตอนToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ออกToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labtransQueueId = new System.Windows.Forms.Label();
            this.RecursiveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ch1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ชองบรการท2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ชองบรการท3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ชองบรการท4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ชองบรการท5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ชองบรการท6ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(299, 234);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "จำนวนคิวที่รอ :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(-2, 234);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(117, 22);
            this.label2.TabIndex = 2;
            this.label2.Text = "จำนวนเก็บคิว  :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnTransfer
            // 
            this.btnTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTransfer.BackColor = System.Drawing.SystemColors.Control;
            this.btnTransfer.Enabled = false;
            this.btnTransfer.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnTransfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransfer.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnTransfer.Image = global::QueueApp.Properties.Resources.swap;
            this.btnTransfer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTransfer.Location = new System.Drawing.Point(8, 192);
            this.btnTransfer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.btnTransfer.Size = new System.Drawing.Size(115, 35);
            this.btnTransfer.TabIndex = 3;
            this.btnTransfer.Text = "โอนคิว";
            this.btnTransfer.UseVisualStyleBackColor = false;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSend.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSend.Enabled = false;
            this.btnSend.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSend.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnSend.Image = global::QueueApp.Properties.Resources.users;
            this.btnSend.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSend.Location = new System.Drawing.Point(126, 192);
            this.btnSend.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSend.Name = "btnSend";
            this.btnSend.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.btnSend.Size = new System.Drawing.Size(115, 35);
            this.btnSend.TabIndex = 4;
            this.btnSend.Text = "ส่งต่อ";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnCencel_Click);
            // 
            // labKeep
            // 
            this.labKeep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labKeep.BackColor = System.Drawing.Color.Transparent;
            this.labKeep.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labKeep.Location = new System.Drawing.Point(113, 234);
            this.labKeep.Name = "labKeep";
            this.labKeep.Size = new System.Drawing.Size(43, 22);
            this.labKeep.TabIndex = 8;
            this.labKeep.Text = "0";
            this.labKeep.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnKeep
            // 
            this.btnKeep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKeep.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnKeep.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnKeep.Enabled = false;
            this.btnKeep.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnKeep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKeep.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnKeep.Image = global::QueueApp.Properties.Resources.save__1_;
            this.btnKeep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKeep.Location = new System.Drawing.Point(244, 192);
            this.btnKeep.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnKeep.Name = "btnKeep";
            this.btnKeep.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.btnKeep.Size = new System.Drawing.Size(115, 35);
            this.btnKeep.TabIndex = 9;
            this.btnKeep.Text = "เก็บคิว";
            this.btnKeep.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnClose.Image = global::QueueApp.Properties.Resources.delete;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(362, 192);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.btnClose.Size = new System.Drawing.Size(115, 35);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "  ปิดระบบ";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Enabled = false;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnCancel.Image = global::QueueApp.Properties.Resources.block;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(362, 152);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.btnCancel.Size = new System.Drawing.Size(115, 35);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "ยกเลิก";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // btnEnd
            // 
            this.btnEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnEnd.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnEnd.Enabled = false;
            this.btnEnd.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnEnd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnd.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnEnd.Image = global::QueueApp.Properties.Resources.stop;
            this.btnEnd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEnd.Location = new System.Drawing.Point(244, 152);
            this.btnEnd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.btnEnd.Size = new System.Drawing.Size(115, 35);
            this.btnEnd.TabIndex = 13;
            this.btnEnd.Text = "จบงาน";
            this.btnEnd.UseVisualStyleBackColor = false;
            // 
            // btnCall
            // 
            this.btnCall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCall.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnCall.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCall.Enabled = false;
            this.btnCall.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnCall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnCall.Image = global::QueueApp.Properties.Resources.accept;
            this.btnCall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCall.Location = new System.Drawing.Point(126, 152);
            this.btnCall.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCall.Name = "btnCall";
            this.btnCall.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.btnCall.Size = new System.Drawing.Size(115, 35);
            this.btnCall.TabIndex = 12;
            this.btnCall.Text = "รับงาน";
            this.btnCall.UseVisualStyleBackColor = false;
            this.btnCall.Click += new System.EventHandler(this.btnCall_Click);
            // 
            // btnRecursive
            // 
            this.btnRecursive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRecursive.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecursive.Enabled = false;
            this.btnRecursive.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnRecursive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecursive.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnRecursive.Image = global::QueueApp.Properties.Resources.refresh;
            this.btnRecursive.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecursive.Location = new System.Drawing.Point(8, 152);
            this.btnRecursive.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecursive.Name = "btnRecursive";
            this.btnRecursive.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.btnRecursive.Size = new System.Drawing.Size(115, 35);
            this.btnRecursive.TabIndex = 11;
            this.btnRecursive.Text = "เรียกซ้ำ";
            this.btnRecursive.UseVisualStyleBackColor = false;
            // 
            // labQueueCode
            // 
            this.labQueueCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labQueueCode.BackColor = System.Drawing.Color.Transparent;
            this.labQueueCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labQueueCode.Font = new System.Drawing.Font("CS ChatThai", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labQueueCode.Location = new System.Drawing.Point(10, 70);
            this.labQueueCode.Name = "labQueueCode";
            this.labQueueCode.Size = new System.Drawing.Size(460, 75);
            this.labQueueCode.TabIndex = 15;
            this.labQueueCode.Text = "2001";
            this.labQueueCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labService
            // 
            this.labService.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labService.BackColor = System.Drawing.Color.Transparent;
            this.labService.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labService.Location = new System.Drawing.Point(126, 38);
            this.labService.Name = "labService";
            this.labService.Size = new System.Drawing.Size(193, 22);
            this.labService.TabIndex = 17;
            this.labService.Text = "ประโยชน์ทดแทน";
            this.labService.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(33, 38);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(94, 22);
            this.label5.TabIndex = 16;
            this.label5.Text = "งานบริการ  :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labKpi
            // 
            this.labKpi.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labKpi.BackColor = System.Drawing.Color.Transparent;
            this.labKpi.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labKpi.Location = new System.Drawing.Point(127, 64);
            this.labKpi.Name = "labKpi";
            this.labKpi.Size = new System.Drawing.Size(133, 22);
            this.labKpi.TabIndex = 19;
            this.labKpi.Text = "10 นาที";
            this.labKpi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label7.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(-6, 64);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label7.Size = new System.Drawing.Size(133, 22);
            this.label7.TabIndex = 18;
            this.label7.Text = "เวลามาตราฐาน  :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(447, 233);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 22);
            this.label8.TabIndex = 20;
            this.label8.Text = "คิว";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // labWait
            // 
            this.labWait.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labWait.BackColor = System.Drawing.Color.Transparent;
            this.labWait.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labWait.Location = new System.Drawing.Point(416, 234);
            this.labWait.Name = "labWait";
            this.labWait.Size = new System.Drawing.Size(34, 22);
            this.labWait.TabIndex = 21;
            this.labWait.Text = "0";
            this.labWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labCounter
            // 
            this.labCounter.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labCounter.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labCounter.Location = new System.Drawing.Point(408, 38);
            this.labCounter.Name = "labCounter";
            this.labCounter.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labCounter.Size = new System.Drawing.Size(68, 22);
            this.labCounter.TabIndex = 23;
            this.labCounter.Text = "1";
            this.labCounter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label10.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(302, 38);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(103, 22);
            this.label10.TabIndex = 22;
            this.label10.Text = "ช่องบริการ  :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labTimeService
            // 
            this.labTimeService.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labTimeService.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labTimeService.Location = new System.Drawing.Point(408, 64);
            this.labTimeService.Name = "labTimeService";
            this.labTimeService.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labTimeService.Size = new System.Drawing.Size(68, 22);
            this.labTimeService.TabIndex = 25;
            this.labTimeService.Text = "15:00";
            this.labTimeService.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label13.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(266, 64);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label13.Size = new System.Drawing.Size(139, 22);
            this.label13.TabIndex = 24;
            this.label13.Text = "เวลาเข้ารับบริการ  :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nextBtn
            // 
            this.nextBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nextBtn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.nextBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nextBtn.Enabled = false;
            this.nextBtn.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.nextBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nextBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.nextBtn.Image = ((System.Drawing.Image)(resources.GetObject("nextBtn.Image")));
            this.nextBtn.Location = new System.Drawing.Point(426, 101);
            this.nextBtn.Name = "nextBtn";
            this.nextBtn.Size = new System.Drawing.Size(50, 31);
            this.nextBtn.TabIndex = 94;
            this.nextBtn.UseVisualStyleBackColor = false;
            this.nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
            // 
            // prevBtn
            // 
            this.prevBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prevBtn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.prevBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.prevBtn.Enabled = false;
            this.prevBtn.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.prevBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.prevBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.prevBtn.Image = ((System.Drawing.Image)(resources.GetObject("prevBtn.Image")));
            this.prevBtn.Location = new System.Drawing.Point(8, 102);
            this.prevBtn.Name = "prevBtn";
            this.prevBtn.Size = new System.Drawing.Size(50, 31);
            this.prevBtn.TabIndex = 95;
            this.prevBtn.UseVisualStyleBackColor = false;
            this.prevBtn.Click += new System.EventHandler(this.prevBtn_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("CS ChatThai", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.callToolStripMenuItem1,
            this.RecursiveToolStripMenuItem1,
            this.EndToolStripMenuItem1,
            this.CancelToolStripMenuItem1,
            this.KeepToolStripMenuItem1,
            this.stopToolStripMenuItem1,
            this.toolStripSeparator3,
            this.รายการเกบควToolStripMenuItem,
            this.toolStripSeparator4,
            this.แจงเตอนToolStripMenuItem1,
            this.toolStripSeparator5,
            this.ออกToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(195, 256);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.SkyBlue;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HotKeyToolStripMenuItem,
            this.เกบควToolStripMenuItem1,
            this.แจงเตอนToolStripMenuItem,
            this.ตงคาToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(484, 30);
            this.menuStrip1.TabIndex = 97;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // HotKeyToolStripMenuItem
            // 
            this.HotKeyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.callToolStripMenuItem,
            this.RecursiveToolStripMenuItem,
            this.EndToolStripMenuItem,
            this.CancelToolStripMenuItem,
            this.KeepToolStripMenuItem,
            this.stopToolStripMenuItem,
            this.toolStripSeparator1,
            this.ปดระบบToolStripMenuItem});
            this.HotKeyToolStripMenuItem.Font = new System.Drawing.Font("CS ChatThai", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.HotKeyToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop;
            this.HotKeyToolStripMenuItem.Name = "HotKeyToolStripMenuItem";
            this.HotKeyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.HotKeyToolStripMenuItem.Size = new System.Drawing.Size(42, 26);
            this.HotKeyToolStripMenuItem.Text = "เมนู";
            // 
            // callToolStripMenuItem
            // 
            this.callToolStripMenuItem.Enabled = false;
            this.callToolStripMenuItem.Name = "callToolStripMenuItem";
            this.callToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.callToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.callToolStripMenuItem.Text = "รับงาน";
            this.callToolStripMenuItem.Click += new System.EventHandler(this.รบงานToolStripMenuItem_Click);
            // 
            // RecursiveToolStripMenuItem
            // 
            this.RecursiveToolStripMenuItem.Enabled = false;
            this.RecursiveToolStripMenuItem.Name = "RecursiveToolStripMenuItem";
            this.RecursiveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.R)));
            this.RecursiveToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.RecursiveToolStripMenuItem.Text = "เรียกซ้ำ";
            // 
            // EndToolStripMenuItem
            // 
            this.EndToolStripMenuItem.Enabled = false;
            this.EndToolStripMenuItem.Name = "EndToolStripMenuItem";
            this.EndToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
            this.EndToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.EndToolStripMenuItem.Text = "จบงาน";
            // 
            // CancelToolStripMenuItem
            // 
            this.CancelToolStripMenuItem.Enabled = false;
            this.CancelToolStripMenuItem.Name = "CancelToolStripMenuItem";
            this.CancelToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D)));
            this.CancelToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.CancelToolStripMenuItem.Text = "ยกเลิกคิว";
            // 
            // KeepToolStripMenuItem
            // 
            this.KeepToolStripMenuItem.Enabled = false;
            this.KeepToolStripMenuItem.Name = "KeepToolStripMenuItem";
            this.KeepToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.K)));
            this.KeepToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.KeepToolStripMenuItem.Text = "เก็บคิว";
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Enabled = false;
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.B)));
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.stopToolStripMenuItem.Text = "หยุดพักบริการ";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(188, 6);
            // 
            // ปดระบบToolStripMenuItem
            // 
            this.ปดระบบToolStripMenuItem.Name = "ปดระบบToolStripMenuItem";
            this.ปดระบบToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.ปดระบบToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.ปดระบบToolStripMenuItem.Text = "ปิดระบบ";
            this.ปดระบบToolStripMenuItem.Click += new System.EventHandler(this.ปดระบบToolStripMenuItem_Click);
            // 
            // เกบควToolStripMenuItem1
            // 
            this.เกบควToolStripMenuItem1.Font = new System.Drawing.Font("CS ChatThai", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.เกบควToolStripMenuItem1.Name = "เกบควToolStripMenuItem1";
            this.เกบควToolStripMenuItem1.Size = new System.Drawing.Size(55, 26);
            this.เกบควToolStripMenuItem1.Text = "เก็บคิว";
            // 
            // ตงคาToolStripMenuItem
            // 
            this.ตงคาToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tooTopMostlStripComboBox1,
            this.autoToolStripMenuItem});
            this.ตงคาToolStripMenuItem.Font = new System.Drawing.Font("CS ChatThai", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ตงคาToolStripMenuItem.Name = "ตงคาToolStripMenuItem";
            this.ตงคาToolStripMenuItem.Size = new System.Drawing.Size(49, 26);
            this.ตงคาToolStripMenuItem.Text = "ตั้งค่า";
            // 
            // tooTopMostlStripComboBox1
            // 
            this.tooTopMostlStripComboBox1.Checked = true;
            this.tooTopMostlStripComboBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tooTopMostlStripComboBox1.Name = "tooTopMostlStripComboBox1";
            this.tooTopMostlStripComboBox1.Size = new System.Drawing.Size(180, 26);
            this.tooTopMostlStripComboBox1.Text = "อยู่หน้าสุดเสมอ";
            this.tooTopMostlStripComboBox1.Click += new System.EventHandler(this.checkToolStripMenuItem_Click);
            // 
            // autoToolStripMenuItem
            // 
            this.autoToolStripMenuItem.Checked = true;
            this.autoToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoToolStripMenuItem.Name = "autoToolStripMenuItem";
            this.autoToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.autoToolStripMenuItem.Text = "รับงานอัตโนมัติ";
            this.autoToolStripMenuItem.Click += new System.EventHandler(this.autoToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkToolStripMenuItem,
            this.toolStripSeparator2,
            this.aToolStripMenuItem});
            this.helpToolStripMenuItem.Font = new System.Drawing.Font("CS ChatThai", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(48, 26);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // checkToolStripMenuItem
            // 
            this.checkToolStripMenuItem.Name = "checkToolStripMenuItem";
            this.checkToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.checkToolStripMenuItem.Text = "Check For Update";
            this.checkToolStripMenuItem.Click += new System.EventHandler(this.checkToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(178, 6);
            // 
            // aToolStripMenuItem
            // 
            this.aToolStripMenuItem.Name = "aToolStripMenuItem";
            this.aToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.aToolStripMenuItem.Text = "About Program";
            this.aToolStripMenuItem.Click += new System.EventHandler(this.aToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(151, 234);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 22);
            this.label3.TabIndex = 98;
            this.label3.Text = "คิว";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // แจงเตอนToolStripMenuItem
            // 
            this.แจงเตอนToolStripMenuItem.Font = new System.Drawing.Font("CS ChatThai", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.แจงเตอนToolStripMenuItem.Name = "แจงเตอนToolStripMenuItem";
            this.แจงเตอนToolStripMenuItem.Size = new System.Drawing.Size(87, 26);
            this.แจงเตอนToolStripMenuItem.Text = "แจ้งเตือน (0)";
            // 
            // callToolStripMenuItem1
            // 
            this.callToolStripMenuItem1.Enabled = false;
            this.callToolStripMenuItem1.Name = "callToolStripMenuItem1";
            this.callToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.callToolStripMenuItem1.Size = new System.Drawing.Size(194, 26);
            this.callToolStripMenuItem1.Text = "รับงาน";
            this.callToolStripMenuItem1.Click += new System.EventHandler(this.callToolStripMenuItem1_Click);
            // 
            // EndToolStripMenuItem1
            // 
            this.EndToolStripMenuItem1.Enabled = false;
            this.EndToolStripMenuItem1.Name = "EndToolStripMenuItem1";
            this.EndToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
            this.EndToolStripMenuItem1.Size = new System.Drawing.Size(194, 26);
            this.EndToolStripMenuItem1.Text = "จบงาน";
            // 
            // CancelToolStripMenuItem1
            // 
            this.CancelToolStripMenuItem1.Enabled = false;
            this.CancelToolStripMenuItem1.Name = "CancelToolStripMenuItem1";
            this.CancelToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D)));
            this.CancelToolStripMenuItem1.Size = new System.Drawing.Size(194, 26);
            this.CancelToolStripMenuItem1.Text = "ยกเลิกคิว";
            // 
            // KeepToolStripMenuItem1
            // 
            this.KeepToolStripMenuItem1.Enabled = false;
            this.KeepToolStripMenuItem1.Name = "KeepToolStripMenuItem1";
            this.KeepToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.K)));
            this.KeepToolStripMenuItem1.Size = new System.Drawing.Size(194, 26);
            this.KeepToolStripMenuItem1.Text = "เก็บคิว";
            // 
            // stopToolStripMenuItem1
            // 
            this.stopToolStripMenuItem1.Enabled = false;
            this.stopToolStripMenuItem1.Name = "stopToolStripMenuItem1";
            this.stopToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.B)));
            this.stopToolStripMenuItem1.Size = new System.Drawing.Size(194, 26);
            this.stopToolStripMenuItem1.Text = "หยุดรับบริการ";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(191, 6);
            // 
            // รายการเกบควToolStripMenuItem
            // 
            this.รายการเกบควToolStripMenuItem.Name = "รายการเกบควToolStripMenuItem";
            this.รายการเกบควToolStripMenuItem.Size = new System.Drawing.Size(194, 26);
            this.รายการเกบควToolStripMenuItem.Text = "รายการเก็บคิว";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(191, 6);
            // 
            // แจงเตอนToolStripMenuItem1
            // 
            this.แจงเตอนToolStripMenuItem1.Name = "แจงเตอนToolStripMenuItem1";
            this.แจงเตอนToolStripMenuItem1.Size = new System.Drawing.Size(194, 26);
            this.แจงเตอนToolStripMenuItem1.Text = "แจ้งเตือน";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(191, 6);
            // 
            // ออกToolStripMenuItem
            // 
            this.ออกToolStripMenuItem.Name = "ออกToolStripMenuItem";
            this.ออกToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.ออกToolStripMenuItem.Size = new System.Drawing.Size(194, 26);
            this.ออกToolStripMenuItem.Text = "ออกจากระบบ";
            this.ออกToolStripMenuItem.Click += new System.EventHandler(this.ออกToolStripMenuItem_Click);
            // 
            // labtransQueueId
            // 
            this.labtransQueueId.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labtransQueueId.BackColor = System.Drawing.Color.Transparent;
            this.labtransQueueId.Font = new System.Drawing.Font("CS ChatThai", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labtransQueueId.Location = new System.Drawing.Point(303, 106);
            this.labtransQueueId.Name = "labtransQueueId";
            this.labtransQueueId.Size = new System.Drawing.Size(117, 22);
            this.labtransQueueId.TabIndex = 99;
            this.labtransQueueId.Text = "trans_queue_id";
            this.labtransQueueId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labtransQueueId.Visible = false;
            // 
            // RecursiveToolStripMenuItem1
            // 
            this.RecursiveToolStripMenuItem1.Enabled = false;
            this.RecursiveToolStripMenuItem1.Name = "RecursiveToolStripMenuItem1";
            this.RecursiveToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.R)));
            this.RecursiveToolStripMenuItem1.Size = new System.Drawing.Size(194, 26);
            this.RecursiveToolStripMenuItem1.Text = "เรียกซ้ำ";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Font = new System.Drawing.Font("CS ChatThai", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ch1ToolStripMenuItem,
            this.ชองบรการท2ToolStripMenuItem,
            this.ชองบรการท3ToolStripMenuItem,
            this.ชองบรการท4ToolStripMenuItem,
            this.ชองบรการท5ToolStripMenuItem,
            this.ชองบรการท6ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(181, 182);
            // 
            // ch1ToolStripMenuItem
            // 
            this.ch1ToolStripMenuItem.Name = "ch1ToolStripMenuItem";
            this.ch1ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.ch1ToolStripMenuItem.Tag = "1";
            this.ch1ToolStripMenuItem.Text = "ช่องบริการที่ 1";
            this.ch1ToolStripMenuItem.Click += new System.EventHandler(this.ch1ToolStripMenuItem_Click);
            // 
            // ชองบรการท2ToolStripMenuItem
            // 
            this.ชองบรการท2ToolStripMenuItem.Name = "ชองบรการท2ToolStripMenuItem";
            this.ชองบรการท2ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.ชองบรการท2ToolStripMenuItem.Text = "ช่องบริการที่ 2";
            // 
            // ชองบรการท3ToolStripMenuItem
            // 
            this.ชองบรการท3ToolStripMenuItem.Name = "ชองบรการท3ToolStripMenuItem";
            this.ชองบรการท3ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.ชองบรการท3ToolStripMenuItem.Text = "ช่องบริการที่ 3";
            // 
            // ชองบรการท4ToolStripMenuItem
            // 
            this.ชองบรการท4ToolStripMenuItem.Name = "ชองบรการท4ToolStripMenuItem";
            this.ชองบรการท4ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.ชองบรการท4ToolStripMenuItem.Text = "ช่องบริการที่ 4";
            // 
            // ชองบรการท5ToolStripMenuItem
            // 
            this.ชองบรการท5ToolStripMenuItem.Name = "ชองบรการท5ToolStripMenuItem";
            this.ชองบรการท5ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.ชองบรการท5ToolStripMenuItem.Text = "ช่องบริการที่ 5";
            // 
            // ชองบรการท6ToolStripMenuItem
            // 
            this.ชองบรการท6ToolStripMenuItem.Name = "ชองบรการท6ToolStripMenuItem";
            this.ชองบรการท6ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.ชองบรการท6ToolStripMenuItem.Text = "ช่องบริการที่ 6";
            // 
            // frmApp
            // 
            this.BackgroundImage = global::QueueApp.Properties.Resources.bg1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(484, 261);
            this.Controls.Add(this.labtransQueueId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.prevBtn);
            this.Controls.Add(this.nextBtn);
            this.Controls.Add(this.labTimeService);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.labCounter);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.labWait);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labKpi);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labService);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnEnd);
            this.Controls.Add(this.btnCall);
            this.Controls.Add(this.btnRecursive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnKeep);
            this.Controls.Add(this.labKeep);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnTransfer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labQueueCode);
            this.Font = new System.Drawing.Font("CS ChatThai", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(500, 300);
            this.MinimumSize = new System.Drawing.Size(500, 245);
            this.Name = "frmApp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบบเรียกคิว";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmApp_FormClosing);
            this.Load += new System.EventHandler(this.QueueApp_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnTransfer;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label labKeep;
        private System.Windows.Forms.Button btnKeep;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnEnd;
        private System.Windows.Forms.Button btnCall;
        private System.Windows.Forms.Button btnRecursive;
        private System.Windows.Forms.Label labQueueCode;
        private System.Windows.Forms.Label labService;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labKpi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labWait;
        private System.Windows.Forms.Label labCounter;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labTimeService;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.Button nextBtn;
        private System.Windows.Forms.Button prevBtn;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem HotKeyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem callToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RecursiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EndToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem KeepToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ปดระบบToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem เกบควToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ตงคาToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tooTopMostlStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem autoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem แจงเตอนToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem callToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem EndToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem CancelToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem KeepToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem รายการเกบควToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem แจงเตอนToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem ออกToolStripMenuItem;
        private System.Windows.Forms.Label labtransQueueId;
        private System.Windows.Forms.ToolStripMenuItem RecursiveToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem ch1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ชองบรการท2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ชองบรการท3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ชองบรการท4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ชองบรการท5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ชองบรการท6ToolStripMenuItem;
    }
}


﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace QueueApp
{
    public partial class frmLogin : Form
    {
        static private List<PrivateFontCollection> _fontCollections;

        public string SERVICE_CHANNEL = "";
        public string AGENCY_CODE = "";
        public string IP_SERVER_WS = "";
        public string POINT_NUMBER = "";


        public frmLogin()
        {
            InitializeComponent();
        }

        static public Font GetCustomFont(string fontFile, float size, FontStyle style)
        {
            if (_fontCollections == null) _fontCollections = new List<PrivateFontCollection>();
            PrivateFontCollection fontCol = new PrivateFontCollection();
            fontCol.AddFontFile(fontFile);
            _fontCollections.Add(fontCol);
            return new Font(fontCol.Families[0], size, style);
        }

        static public Font GetCustomFont(byte[] fontData, float size, FontStyle style)
        {
            if (_fontCollections == null) _fontCollections = new List<PrivateFontCollection>();
            PrivateFontCollection fontCol = new PrivateFontCollection();
            IntPtr fontPtr = Marshal.AllocCoTaskMem(fontData.Length);
            Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            fontCol.AddMemoryFont(fontPtr, fontData.Length);
            Marshal.FreeCoTaskMem(fontPtr);     //<-- It works!
            _fontCollections.Add(fontCol);
            return new Font(fontCol.Families[0], size, style);
        }



        private void QueueApp_Load(object sender, EventArgs e)
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            var settings = configFile.AppSettings.Settings;

            SERVICE_CHANNEL = settings["SERVICE_CHANNEL"].Value;
            AGENCY_CODE = settings["AGENCY_CODE"].Value;
            IP_SERVER_WS = settings["IP_SERVER_WS"].Value;
            POINT_NUMBER = settings["POINT_NUMBER"].Value; 


            PrivateFontCollection pfc = new PrivateFontCollection();

            pfc.AddFontFile("CSChatThai.ttf");

            this.Font   = new Font(pfc.Families[0], 15, FontStyle.Regular);
            label1.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            label2.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            label3.Font = new Font(pfc.Families[0], 18, FontStyle.Bold);
            label4.Font = new Font(pfc.Families[0], 18, FontStyle.Bold);
            label5.Font = new Font(pfc.Families[0], 14, FontStyle.Regular);
            textBox1.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            btnSubmit.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            btnCencel.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);

            string version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            this.Text = "ระบบบริหารจัดการคิว " + version + "    ช่องบริการ " + SERVICE_CHANNEL;


            //this.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //textBox1.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //label1.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //label2.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //label3.Font = GetCustomFont(Properties.Resources.CSChatThai, 18.75F, FontStyle.Bold);
            //label4.Font = GetCustomFont(Properties.Resources.CSChatThai, 18.75F, FontStyle.Bold);
            //btnSubmit.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //btnCencel.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);


        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text.Equals(""))
                {
                    textBox1.Focus();
                    return;
                }
                else if (textBox2.Text.Equals(""))
                {
                    textBox2.Focus();
                    return;
                }

                string user = textBox1.Text;
                string pw = textBox2.Text;

                string url = IP_SERVER_WS + "/ws/service.php";


                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"functionName\":\"userLogin\"," +
                                  "\"dataJson\":{\"user\":\"" + user + "\",\"pw\":\"" + pw + "\",\"acode\":\"" + AGENCY_CODE + "\",\"sc\":\"" + SERVICE_CHANNEL + "\",\"p\":\"" + POINT_NUMBER + "\"}}";

                    streamWriter.Write(json);
                }


                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();



                    JavaScriptSerializer js = new JavaScriptSerializer();
                    ResponseUser responseData = js.Deserialize<ResponseUser>(result);

                    if (responseData.status == "200")
                    {


                        frmApp FrmApp = new frmApp();
                        FrmApp.userName = responseData.data.user_name;
                        FrmApp.refCode  = responseData.data.ref_code;
                        FrmApp.point_id = responseData.data.point_id;
                        FrmApp.service_id_list = responseData.data.service_id_list;
                        FrmApp.is_auto_service = responseData.data.is_auto_service;
                        FrmApp.Show();
                        this.Hide();
                    }
                    else if (responseData.status == "403")
                    {
                        label5.Text = responseData.message;
                        label5.Visible = true;
                    }
                    else
                    {
                        label5.Text = "ไม่พบรหัสผู้ใช้งาน";
                        label5.Visible = true;
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox1.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmSetting frmSetting = new frmSetting(this);
            frmSetting.Show();
            this.Hide();

        }

        private void btnCencel_Click(object sender, EventArgs e)
        {
            Utils.closeApp();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            label5.Visible = false;
        }
    }
}

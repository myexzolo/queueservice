﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueApp
{
    class ResponseQueue
    {
        public  string status { get; set; }
        public  string message { get; set; }
        public  Queue[] data { get; set; }
        public  string functionName { get; set; }
    }
}

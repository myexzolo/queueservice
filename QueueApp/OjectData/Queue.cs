﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueApp
{
    class Queue
    {
        public string trans_queue_id { get; set; }
        public string queue_code { get; set; }
        public string ref_code { get; set; }
        public string date_start { get; set; }
        public string type_service { get; set; }
        public string menu_id { get; set; }
        public string menu_name { get; set; }
        public string service_name_a { get; set; }
        public string kpi_time_a { get; set; }
    }
}

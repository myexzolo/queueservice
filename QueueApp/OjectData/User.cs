﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueApp
{
    class User
    {
        public string user_name { get; set; }
        public string ref_code { get; set; }
        public string point_id { get; set; }
        public string service_id_list { get; set; }
        public string is_auto_service { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueApp
{
    class ResponseUser
    {
        public  string status { get; set; }
        public  string message { get; set; }
        public  User data { get; set; }
        public  string functionName { get; set; }
    }
}

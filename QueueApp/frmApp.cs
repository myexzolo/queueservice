﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace QueueApp
{
    public partial class frmApp : Form
    {
        static private List<PrivateFontCollection> _fontCollections;

        public string SERVICE_CHANNEL = "";
        public string AGENCY_CODE = "";
        public string IP_SERVER_WS = "";
        public string POINT_NUMBER = "";

        public string userName = "";
        public string refCode  = "";
        public string point_id = "";
        public string service_id_list = "";
        public string is_auto_service = "";

        private string MENU_ID = "";

        private string status_active = "Y";

        private Thread clientThread = null;

        private string SKIP = "";

        public frmApp()
        {

            InitializeComponent();

            if (is_auto_service.Equals("Y"))
            {
                this.autoToolStripMenuItem.Checked = true;
            }
            else
            {
                this.autoToolStripMenuItem.Checked = false;
            }

            if (this.tooTopMostlStripComboBox1.Checked)
            {
                this.TopMost = true;
            }
            else
            {
                this.tooTopMostlStripComboBox1.Checked = false;
                this.TopMost = false;
            }
        }

        static public Font GetCustomFont(string fontFile, float size, FontStyle style)
        {
            if (_fontCollections == null) _fontCollections = new List<PrivateFontCollection>();
            PrivateFontCollection fontCol = new PrivateFontCollection();
            fontCol.AddFontFile(fontFile);
            _fontCollections.Add(fontCol);
            return new Font(fontCol.Families[0], size, style);
        }

        static public Font GetCustomFont(byte[] fontData, float size, FontStyle style)
        {
            if (_fontCollections == null) _fontCollections = new List<PrivateFontCollection>();
            PrivateFontCollection fontCol = new PrivateFontCollection();
            IntPtr fontPtr = Marshal.AllocCoTaskMem(fontData.Length);
            Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            fontCol.AddMemoryFont(fontPtr, fontData.Length);
            Marshal.FreeCoTaskMem(fontPtr);     //<-- It works!
            _fontCollections.Add(fontCol);
            return new Font(fontCol.Families[0], size, style);
        }



        private void QueueApp_Load(object sender, EventArgs e)
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            var settings = configFile.AppSettings.Settings;

            SERVICE_CHANNEL = settings["SERVICE_CHANNEL"].Value;
            AGENCY_CODE = settings["AGENCY_CODE"].Value;
            IP_SERVER_WS = settings["IP_SERVER_WS"].Value;
            POINT_NUMBER = settings["POINT_NUMBER"].Value;


            Rectangle bounds = Screen.PrimaryScreen.WorkingArea;
            this.Location = new System.Drawing.Point(bounds.Width - 500, bounds.Height - 300);

            PrivateFontCollection pfc = new PrivateFontCollection();

            pfc.AddFontFile("CSChatThai.ttf");

            this.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            label1.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            label2.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            labService.Font = new Font(pfc.Families[0], 14, FontStyle.Regular);
            labKeep.Font = new Font(pfc.Families[0], 14, FontStyle.Regular);
            label5.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            labKpi.Font = new Font(pfc.Families[0], 14, FontStyle.Regular);
            label7.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            label8.Font = new Font(pfc.Families[0], 14, FontStyle.Regular);
            labWait.Font = new Font(pfc.Families[0], 14, FontStyle.Regular);
            label10.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            labCounter.Font = new Font(pfc.Families[0], 14, FontStyle.Regular);
            labTimeService.Font = new Font(pfc.Families[0], 14, FontStyle.Regular);
            label13.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            btnTransfer.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            btnSend.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            btnKeep.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            btnClose.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            btnCancel.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            btnEnd.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            btnCall.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            btnRecursive.Font = new Font(pfc.Families[0], 14, FontStyle.Bold);
            labQueueCode.Font = new Font(pfc.Families[0], 60, FontStyle.Bold);

            labQueueCode.Font = new Font(pfc.Families[0], 60, FontStyle.Bold);




            string version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            this.Text = "ระบบบริหารจัดการคิว " + version + " (" + userName + ")";

            labCounter.Text = SERVICE_CHANNEL;

            labQueueCode.Text = "";
            labService.Text = "";
            labKpi.Text = "";
            labTimeService.Text = "";
            labWait.Text = "0";
            labKeep.Text = "0";

            this.clientThread = new Thread(new ThreadStart(this.RunProcess));
            this.clientThread.Start();
        }

        private async void RunProcess()
        {
            while (true)
            {
                try
                {
                    if (status_active.Equals("Y"))
                    {
                        await getqueue();
                    }
                    else 
                    {
                        getqueueWait();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally {
                    Thread.Sleep(1000);
                }   
            }
        }

        private async Task getqueue()
        {
            try
            {
                string url = IP_SERVER_WS + "/ws/service.php";


                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"functionName\":\"getQueue\"," + 
                                  "\"dataJson\":{\"p\":\"" + point_id + "\",\"sl\":\"" + service_id_list + "\",\"acode\":\"" + AGENCY_CODE + "\",\"skip\":\"" + SKIP + "\"}}";

                    streamWriter.Write(json);
                }


                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using(var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    ResponseQueue responseData = js.Deserialize<ResponseQueue>(result);

                    if (responseData.status == "200")
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {
                                Queue[] queueArr = responseData.data;


                                Queue qdata = queueArr[0];

                                var date = DateTime.Parse(qdata.date_start, new CultureInfo("th-TH", true));

                                string kpiStr = (String.IsNullOrEmpty(qdata.kpi_time_a) ? "" : qdata.kpi_time_a + " นาที");

                                labQueueCode.Text       = qdata.queue_code;
                                labTimeService.Text     = date.ToString("HH:mm");
                                labKpi.Text             = kpiStr;
                                labtransQueueId.Text    = qdata.trans_queue_id;
                                string type_service = qdata.type_service;

                                if (type_service.Equals("1")) 
                                {
                                    labService.Text = qdata.menu_name;
                                }
                                else 
                                {
                                    labService.Text = qdata.service_name_a;
                                }

                                int numWait = queueArr.Count();

                                if (numWait > 0)
                                {
                                    btnCall.Enabled = true;
                                    callToolStripMenuItem.Enabled = true;
                                    callToolStripMenuItem1.Enabled = true;
                                }
                                else 
                                {
                                    btnCall.Enabled = false;
                                    callToolStripMenuItem.Enabled = false;
                                    callToolStripMenuItem1.Enabled = false;
                                }

                                if (numWait > 1)
                                {
                                    nextBtn.Enabled = true;
                                    
                                }
                                else {
                                    nextBtn.Enabled = false;
                                }

                                int skipnum = 0;

                                if (!SKIP.Equals(""))
                                {
                                    prevBtn.Enabled = true;
                                    skipnum  = SKIP.Split(',').Count();
                                }
                                else {
                                    prevBtn.Enabled = false;
                                }
                                labWait.Text = (numWait + skipnum - 1).ToString();
                            }));
                        }
                        else
                        {
                            
                        }
                    }
                    else if (responseData.status == "201")
                    {
                        SKIP = "";

                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {
                                nextBtn.Enabled = false;
                                prevBtn.Enabled = false;

                                btnCall.Enabled = false;
                                callToolStripMenuItem.Enabled = false;
                                callToolStripMenuItem1.Enabled = false;

                                labWait.Text = "0";
                            }));
                        }
                        else
                        {

                        }

                    }
                    else
                    {

                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                await Task.Delay(100);
            }
        }




        private void getqueueWait()
        {
            try
            {
                string url = IP_SERVER_WS + "/ws/service.php";


                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"functionName\":\"getQueue\"," +
                                  "\"dataJson\":{\"p\":\"" + point_id + "\",\"sl\":\"" + service_id_list + "\",\"acode\":\"" + AGENCY_CODE + "\",\"skip\":\"" + SKIP + "\"}}";

                    streamWriter.Write(json);
                }


                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    ResponseQueue responseData = js.Deserialize<ResponseQueue>(result);

                    if (responseData.status == "200")
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {
                                Queue[] queueArr = responseData.data;


                                Queue qdata = queueArr[0];

                                int numWait = queueArr.Count();
                                labWait.Text = (numWait - 1).ToString();
                            }));
                        }
                        else
                        {

                        }
                    }
                    else if (responseData.status == "201")
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {
                                labWait.Text = "0";
                            }));
                        }
                        else
                        {

                        }

                    }
                    else
                    {

                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {

            }
        }


        private async Task callqueue()
        {
            try
            {
                string url = IP_SERVER_WS + "/ws/service.php";


                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"functionName\":\"callQueue\"," +
                                  "\"dataJson\":{\"p\":\"" + point_id + "\",\"sl\":\"" + service_id_list + "\",\"acode\":\"" + AGENCY_CODE + "\",\"qid\":\"" + labtransQueueId.Text + "\"}}";

                    streamWriter.Write(json);
                }


                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    ResponseQueue responseData = js.Deserialize<ResponseQueue>(result);

                    if (responseData.status == "200")
                    {
                        SKIP = "";
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {
                                Queue[] queueArr = responseData.data;


                                Queue qdata = queueArr[0];

                                var date = DateTime.Parse(qdata.date_start, new CultureInfo("th-TH", true));

                                string kpiStr = (String.IsNullOrEmpty(qdata.kpi_time_a) ? "" : qdata.kpi_time_a + " นาที");

                                labQueueCode.Text = qdata.queue_code;
                                labTimeService.Text = date.ToString("HH:mm");
                                labKpi.Text = kpiStr ;
                                labtransQueueId.Text = qdata.trans_queue_id;
                                string type_service = qdata.type_service;

                                if (type_service.Equals("1"))
                                {
                                    labService.Text = qdata.menu_name;
                                }
                                else
                                {
                                    labService.Text = qdata.service_name_a;
                                }

                                int numWait = queueArr.Count();


                                btnCall.Enabled = false;
                                callToolStripMenuItem.Enabled = false;
                                callToolStripMenuItem1.Enabled = false;

                                nextBtn.Enabled = false;
                                prevBtn.Enabled = false;

                                stopToolStripMenuItem.Enabled = false;
                                stopToolStripMenuItem1.Enabled = false;

                                labWait.Text = (numWait - 1).ToString();
                            }));
                        }
                        else
                        {
                            Queue[] queueArr = responseData.data;


                            Queue qdata = queueArr[0];

                            var date = DateTime.Parse(qdata.date_start, new CultureInfo("th-TH", true));

                            string kpiStr = (String.IsNullOrEmpty(qdata.kpi_time_a) ? "" : qdata.kpi_time_a + " นาที");

                            labQueueCode.Text = qdata.queue_code;
                            labTimeService.Text = date.ToString("HH:mm");
                            labKpi.Text = kpiStr;
                            labtransQueueId.Text = qdata.trans_queue_id;
                            string type_service = qdata.type_service;

                            if (type_service.Equals("1"))
                            {
                                labService.Text = qdata.menu_name;
                            }
                            else
                            {
                                labService.Text = qdata.service_name_a;
                            }

                            int numWait = queueArr.Count();


                            btnCall.Enabled     = false;
                            callToolStripMenuItem.Enabled = false;
                            callToolStripMenuItem1.Enabled = false;

                            nextBtn.Enabled     = false;
                            prevBtn.Enabled     = false;
                            btnClose.Enabled   = false;

                            btnRecursive.Enabled    = true;
                            RecursiveToolStripMenuItem.Enabled = true;
                            RecursiveToolStripMenuItem1.Enabled = true;

                            btnEnd.Enabled          = true;
                            EndToolStripMenuItem.Enabled = true;
                            EndToolStripMenuItem1.Enabled = true;

                            btnCancel.Enabled       = true;
                            CancelToolStripMenuItem.Enabled = true;
                            CancelToolStripMenuItem1.Enabled = true;

                            btnTransfer.Enabled     = true;

                            btnSend.Enabled         = true;

                            btnKeep.Enabled         = true;
                            KeepToolStripMenuItem.Enabled = true;
                            KeepToolStripMenuItem1.Enabled = true;

                            stopToolStripMenuItem.Enabled = false;
                            stopToolStripMenuItem1.Enabled = false;

                            labWait.Text = (numWait - 1).ToString();
                        }
                    }
                    else if (responseData.status == "201")
                    {
                        SKIP = "";

                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {
                                nextBtn.Enabled = false;
                                prevBtn.Enabled = false;

                                btnCall.Enabled = false;
                                callToolStripMenuItem.Enabled = false;
                                callToolStripMenuItem1.Enabled = false;
                            }));
                        }
                        else
                        {
                            nextBtn.Enabled = false;
                            prevBtn.Enabled = false;

                            btnCall.Enabled = false;
                            callToolStripMenuItem.Enabled = false;
                            callToolStripMenuItem1.Enabled = false;
                        }

                    }
                    else
                    {

                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                await Task.Delay(100);
            }
        }

        private void btnCencel_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Utils.closeApp();
        }

        private void frmApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            Utils.closeApp();
        }

        private void checkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    if (((ToolStripMenuItem)sender).CheckState == CheckState.Checked)
                    {
                        ((ToolStripMenuItem)sender).CheckState = CheckState.Unchecked;
                        this.TopMost = Convert.ToBoolean(false);
                    }
                    else
                    {
                        ((ToolStripMenuItem)sender).CheckState = CheckState.Checked;
                        this.TopMost = Convert.ToBoolean(true);
                    }
                }));
            }
            else
            {
                if (((ToolStripMenuItem)sender).CheckState == CheckState.Checked)
                {
                    ((ToolStripMenuItem)sender).CheckState = CheckState.Unchecked;
                    this.TopMost = Convert.ToBoolean(false);
                }
                else
                {
                    ((ToolStripMenuItem)sender).CheckState = CheckState.Checked;
                    this.TopMost = Convert.ToBoolean(true);
                }
            }
        }

        private void aToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ปดระบบToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Utils.closeApp();
        }

        private async void รบงานToolStripMenuItem_Click(object sender, EventArgs e)
        {
            status_active = "N";
            await callqueue();
        }

        private void ออกToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void autoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    if (((ToolStripMenuItem)sender).CheckState == CheckState.Checked)
                    {
                        ((ToolStripMenuItem)sender).CheckState = CheckState.Unchecked;
                        is_auto_service = "N";
                    }
                    else
                    {
                        ((ToolStripMenuItem)sender).CheckState = CheckState.Checked;
                        this.TopMost = Convert.ToBoolean(true);
                        is_auto_service = "Y";
                    }
                }));
            }
            else
            {
                if (((ToolStripMenuItem)sender).CheckState == CheckState.Checked)
                {
                    ((ToolStripMenuItem)sender).CheckState = CheckState.Unchecked;
                    is_auto_service = "N";
                }
                else
                {
                    ((ToolStripMenuItem)sender).CheckState = CheckState.Checked;
                    this.TopMost = Convert.ToBoolean(true);
                    is_auto_service = "Y";
                }
            }
        }

        private async void nextBtn_Click(object sender, EventArgs e)
        {
            SKIP += (SKIP.Length > 0 ? "," : "") + labtransQueueId.Text;
            await getqueue();
        }

        private async void prevBtn_Click(object sender, EventArgs e)
        {
            String[] skipArr = SKIP.Split(',');

            List<string> list = new List<string>(skipArr);

            list.RemoveAt(skipArr.Count() - 1);
            SKIP = "";
            for (int x = 0; x < list.Count(); x++)
            {
                SKIP += (SKIP.Length > 0 ? "," : "") + list[x];
            }

            await getqueue();
        }

        private async void btnCall_Click(object sender, EventArgs e)
        {
            status_active = "N";
            await callqueue();
        }

        private async void callToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            status_active = "N";
            await callqueue();

        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            contextMenuStrip2.Show(new Point(MousePosition.X, MousePosition.Y));
        }

        private void ch1ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}

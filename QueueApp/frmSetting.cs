﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QueueApp
{
    public partial class frmSetting : Form
    {
        static private List<PrivateFontCollection> _fontCollections;

        public string SERVICE_CHANNEL   = System.Configuration.ConfigurationManager.AppSettings["SERVICE_CHANNEL"];
        public string AGENCY_CODE       = System.Configuration.ConfigurationManager.AppSettings["AGENCY_CODE"];
        public string IP_SERVER_WS      = System.Configuration.ConfigurationManager.AppSettings["IP_SERVER_WS"];
        public string POINT_NUMBER      = System.Configuration.ConfigurationManager.AppSettings["POINT_NUMBER"];
        

        public frmLogin login = new frmLogin();
        public frmSetting(frmLogin login)
        {
            InitializeComponent();
        }

        static public Font GetCustomFont(string fontFile, float size, FontStyle style)
        {
            if (_fontCollections == null) _fontCollections = new List<PrivateFontCollection>();
            PrivateFontCollection fontCol = new PrivateFontCollection();
            fontCol.AddFontFile(fontFile);
            _fontCollections.Add(fontCol);
            return new Font(fontCol.Families[0], size, style);
        }

        static public Font GetCustomFont(byte[] fontData, float size, FontStyle style)
        {
            if (_fontCollections == null) _fontCollections = new List<PrivateFontCollection>();
            PrivateFontCollection fontCol = new PrivateFontCollection();
            IntPtr fontPtr = Marshal.AllocCoTaskMem(fontData.Length);
            Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            fontCol.AddMemoryFont(fontPtr, fontData.Length);
            Marshal.FreeCoTaskMem(fontPtr);     //<-- It works!
            _fontCollections.Add(fontCol);
            return new Font(fontCol.Families[0], size, style);
        }



        private void QueueApp_Load(object sender, EventArgs e)
        {
            PrivateFontCollection pfc = new PrivateFontCollection();

            pfc.AddFontFile("CSChatThai.ttf");

            //this.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //textBox1.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //textBox2.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //textBox3.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //label1.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //label2.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //label3.Font = GetCustomFont(Properties.Resources.CSChatThai, 18.75F, FontStyle.Bold);
            //label4.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //label5.Font = GetCustomFont(Properties.Resources.CSChatThai, 18.75F, FontStyle.Bold);
            //btnSubmit.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);
            //btnCencel.Font = GetCustomFont(Properties.Resources.CSChatThai, 15F, FontStyle.Regular);


            this.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            label1.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            label2.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            label3.Font = new Font(pfc.Families[0], 18, FontStyle.Bold);
            label4.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            label5.Font = new Font(pfc.Families[0], 18, FontStyle.Bold);
            label6.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            textBox1.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            textBox2.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            textBox3.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            textBox4.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            btnSubmit.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);
            btnCencel.Font = new Font(pfc.Families[0], 15, FontStyle.Regular);


            textBox1.Text = SERVICE_CHANNEL;
            textBox2.Text = IP_SERVER_WS;
            textBox3.Text = AGENCY_CODE;
            textBox4.Text = POINT_NUMBER;
            textBox2.Text = IP_SERVER_WS;

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings["SERVICE_CHANNEL"] == null)
                {
                    settings.Add("SERVICE_CHANNEL", "");
                }
                else
                {
                    settings["SERVICE_CHANNEL"].Value = textBox1.Text;
                }

                if (settings["AGENCY_CODE"] == null)
                {
                    settings.Add("AGENCY_CODE", "");
                }
                else
                {
                    settings["AGENCY_CODE"].Value = textBox3.Text;
                }

                if (settings["IP_SERVER_WS"] == null)
                {
                    settings.Add("IP_SERVER_WS", "");
                }
                else
                {
                    settings["IP_SERVER_WS"].Value = textBox2.Text;
                }

                if (settings["POINT_NUMBER"] == null)
                {
                    settings.Add("POINT_NUMBER", "");
                }
                else
                {
                    settings["POINT_NUMBER"].Value = textBox4.Text;
                }

                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
            finally {
                login.Show();
                this.Close();
            }
        }

        private void btnCencel_Click(object sender, EventArgs e)
        {
            login.Show();
            this.Close();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
